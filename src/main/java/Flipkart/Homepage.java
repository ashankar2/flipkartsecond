package Flipkart;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Homepage {
	
	@Test
	public void homeflipkart() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//Close Login Screen Pop-up
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		
		//Mouse hover on Electronics
		Actions builder = new Actions(driver);
		WebElement electronics = driver.findElementByXPath("//span[text()='Electronics']");
		builder.moveToElement(electronics).perform();
		
		//Click on Mi/RealMe
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Mi"))).click();	
		
		//Verify Title
		Thread.sleep(5000);
		String mititle = driver.getTitle();
		if(mititle.contains("Mi Mobile Phones"))
			System.out.println("Verified Title: "+mititle);
		else
			System.out.println("Wrong page");
		
		//Click on Newest First
		driver.findElementByXPath("//div[text()='Newest First']").click();
		
		//Print all product name and price
		Thread.sleep(5000);
		List<WebElement> productname = driver.findElementsByXPath("//div[@class='_3wU53n']");
		List<WebElement> productprice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for (int i = 0; i < productname.size(); i++) {
			
			System.out.println(productname.get(i).getText()+" : "+productprice.get(i).getText().replaceAll("//D", ""));
			
			
		}
		
		//Click on first product
		Thread.sleep(3000);
		String fpname = driver.findElementByXPath("//div[@class='_3wU53n']").getText();
		driver.findElementByXPath("//div[@class='_3wU53n']").click();
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='_3wU53n']"))).click();
		
		//Switch to window
		Set<String> winset = driver.getWindowHandles();
		List<String> winlist = new ArrayList<>();
		winlist.addAll(winset);
		driver.switchTo().window(winlist.get(1));
		
		//Verify First product page
		String fptitle = driver.getTitle();
		if(fptitle.contains(fpname))
			System.out.println("Verified Product Title: "+fptitle);
		else
			System.out.println("Wrong product");
		
		//Count Ratings and Review
		String review = driver.findElementByXPath("//span[@class='_38sUEc']//span//span[text()[contains(.,'Reviews')]]").getText();
		String rating = driver.findElementByXPath("//span[@class='_38sUEc']//span//span[text()[contains(.,'Ratings')]]").getText();
		System.out.println(fpname+" reviews :"+review);
		System.out.println(fpname+" ratings :"+rating);
		
		//Browser Close
		driver.quit();
		
	}

}
